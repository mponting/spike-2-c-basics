#include "Game.h"

using namespace std;

//Starts the Game by randomising a new number to guess.
Game::Game()
{
	answer = 1 + rand() % 100;
}

Game::Game(int specific_answer) : answer(specific_answer)
{

}

void Game::MakeMove()
{
	cout << "Guess a number between 1 and 100.\n";
	int guess = 0;
	string answertext = "owusjashfdfsj";
	cin >> answertext;

	if (answertext == "owusjashfdfsj") {
		cout << "Answer is not valid! Please try again!" << endl;
		cin.clear();
		return;
	}

	//Goes through each character of the inputted answer and makes sure it is a number.
	//If anything else is found, it will automatically deny it.
	for (unsigned i = 0; i < answertext.length(); ++i) {
		char character = answertext.at(i);
		if (!isdigit(character)) {
			cout << "Answer is not valid! Please try again!" << endl;
			cin.clear();
			return;
		}
	}

	guess = stoi(answertext);
	guesses[attempts] = guess;
	 //Adds it to gueses
	//Takes the guess from the guesses list and checks it.

	if (guess > 100 || guess < 1) {
		cout << "Please enter a number between 1 and 100. Not above or below." << endl;
		cin.clear();
		return;
	}

	if (guess == answer) {
		cout << "You have guessed the correct number!" << endl;
		Finish();
	}
	
	else
	{
		if (attempts == 9) {
			cout << "You have used up all your guesses!" << endl;
			Finish();
		}
		if (guess < answer) {
			cout << "You need to go higher!" << endl;
		}

		else {
			cout << "You need to go lower!" << endl;
		}
		attempts++;
	}

}

//Shuts down the game and shows the guesses that a player has made.
void Game::Finish()
{
	finished = true;
	for (int i = 0; i <= attempts; i++) {
		cout << i << ". " << guesses[i] << endl;
	}
}