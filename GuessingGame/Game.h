#pragma once

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include "Source.h"

class Game {

public:
	bool finished = false;
	Game();
	Game(int answer);
	void MakeMove();

private:
	int answer = 0;
	int guesses[10];
	int attempts = 0;

	void Finish();
};
