#include <iostream>
#include <string>
#include "Source.h"
#include "Game.h"
#include <random>

//using namespace std;

//Functions
void PlayGame();

int main()
{
	std::minstd_rand
	srand(time(0));
	PlayGame();

	system("pause");
	return 0;
}

void PlayGame()
{
	//Creates the game and plays it while it isn't finished.
	Game gameReference;

	while (gameReference.finished == false) {
		gameReference.MakeMove();
	}

	//End of the game, prompts if they wish to play again.
	std::cin.clear();
	std::string answer;

	std::cout << "The game has ended! Do you wish to play again? (Y/N) (Other answers will exit the game). \n";
	std::cin >> answer;

	if (answer == "y" || answer == "Y") {
		PlayGame();
	}

	else {
		return;
	}
}