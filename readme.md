| **Spike ID**        | B2                                                      | **Time Needed** | 6 hours |
|---------------------|---------------------------------------------------------|-----------------|---------|
| **Title**           | Base Spike 2 – C++ Basics                               |
| **Personnel**       | Mathew Ponting (blueporgrammer)                         |
| **Repository Link** | <https://bitbucket.org/blueprogrammer/spike-2-c-basics> |

Introduction
============

In this spike, the programmer will learn basic C++ programming using the
solo learn C++ website/app. Afterwards, the programmer will demonstrate
their new programming knowledge by making a numerical guessing game
written in visual studio.

Goal
====

Develop a basic numerical guessing game that requires you to guess
numbers between 1 and 100 and at the end show the number of guesses the
user made.

TRT (Technology, Resources and Tools)
=====================================

| **Resource**                                                     | **Needed/Recommended** |
|------------------------------------------------------------------|------------------------|
| Visual Studio (Or any other C++ Compiler, Visual Studio is free) | Needed                 |
| Sololearn C++                                                    | Needed                 |
| Cppreference.com                                                 |                        |

Tasks
=====

1.  Create a brand new blank C++ project.

2.  Create main() that calls a function called Game(). Since main() has
    to return a int, just make it return 0.

3.  Create a PlayGame() Function. Make a while loop that links to a
    variable in the Game Class that represents if the game is finished
    or not and a function called MakeMove().

4.  Create a brand new cpp file (with a header file as well) and call it
    Game and set it up as a class (Called Game). This is going to hold
    all of our variables such as our guesses, attempts, the answer and
    also all the methods that are going to make the game function
    (MakeMove(), Finish()).

5.  In main(),Add the following function: srand(time(0)). This will
    allow sufficiently random numbers to pop up when we go to generate
    the answer.

6.  In the Game Class constructor, randomize an answer and store it in a
    variable (Between 1 and 100 to be specific).

7.  Still in the game class in MakeMove(), ask for an input then make it
    equal into a string variable. Then use that string to check each
    character in the string and make sure that they are numeric (use the
    isdigit function) and not anything else. If it’s a non-numeric
    answer, make the player try again.

8.  Make a validator to make sure the number that you enter is above 0
    and below 100. If its not, clear the input and return.
    Otherwise, continue.

9.  Convert the string to a int variable and check if the int is equal
    to the answer and if it is call Finish(), however if it’s not, then
    check if the answer is higher or lower than the actual answer.

Discovered
==========

-   C++ has access to object oriented programming but often
    not recommended. (From Future Spikes).

-   A function within a class is called a method.

-   A method within a class that is named the same as it’s class name is
    called a constructor and it is called when the class is created.

-   Constructors are really important when defining what that class
    can do. As in this method, you can set the unique values of
    that object.

-   Using srand(time(0)) will allow the use of sufficiently
    random numbers. This is because the time(0) function is a good way
    to get a random integer that isn’t fixed.

-   Cin.Fail() is a good way to make sure the input can fit into the
    data type of the variable you are inputted to.

-   Using isdigit will allow you to check if the character you pass in
    is a digit. Use it with combination with a for loop to check if your
    whole string is a numeric.

Recommendations
===============

-   The link provided in the resources file for a .gitignore is only if
    you were to use a git repository to store this project. But I
    absolutely recommend using this as it will give you an understanding
    on how git works in programming projects.

-   Finish the Sololearn tutorials after this project, it will be
    helpful for future projects.

-   Comment on your code. Make sure other people can understand it and
    know what is going on within the function.

-   If throughout the tasks undertaken you do not understand what needs
    to be done, you should go back onto the sololearn website and check
    there on how to do things.

-   Try to split the Game class and the main function from each other in
    the project. If you don’t, things can get messy and unreadable.

-   Make sure your variable naming makes sense.

